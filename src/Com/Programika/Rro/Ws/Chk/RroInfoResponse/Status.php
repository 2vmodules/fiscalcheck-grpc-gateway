<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: Check.proto

namespace Com\Programika\Rro\Ws\Chk\RroInfoResponse;

/**
 * Protobuf type <code>com.programika.rro.ws.chk.RroInfoResponse.Status</code>
 */
class Status
{
    /**
     * Generated from protobuf enum <code>UNKNOWN = 0;</code>
     */
    const UNKNOWN = 0;
    /**
     * Generated from protobuf enum <code>OK = 1;</code>
     */
    const OK = 1;
    /**
     * Generated from protobuf enum <code>ERROR_VEREFY = -1;</code>
     */
    const ERROR_VEREFY = -1;
    /**
     * Generated from protobuf enum <code>ERROR_CHECK = -2;</code>
     */
    const ERROR_CHECK = -2;
    /**
     * Generated from protobuf enum <code>ERROR_UNKNOWN = -4;</code>
     */
    const ERROR_UNKNOWN = -4;
    /**
     * Generated from protobuf enum <code>ERROR_NOT_REGISTERED_RRO = -13;</code>
     */
    const ERROR_NOT_REGISTERED_RRO = -13;
    /**
     * Generated from protobuf enum <code>ERROR_NOT_REGISTERED_SIGNER = -14;</code>
     */
    const ERROR_NOT_REGISTERED_SIGNER = -14;
}

// Adding a class alias for backwards compatibility with the previous class name.
class_alias(Status::class, \Com\Programika\Rro\Ws\Chk\RroInfoResponse_Status::class);

