<?php
// GENERATED CODE -- DO NOT EDIT!

namespace Com\Programika\Rro\Ws\Chk;

/**
 */
class ChkIncomeServiceClient extends \Grpc\BaseStub {

    /**
     * @param string $hostname hostname
     * @param array $opts channel options
     * @param \Grpc\Channel $channel (optional) re-use channel object
     */
    public function __construct($hostname, $opts, $channel = null) {
        parent::__construct($hostname, $opts, $channel);
    }

    /**
     * @param \Com\Programika\Rro\Ws\Chk\Check $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function sendChk(\Com\Programika\Rro\Ws\Chk\Check $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/com.programika.rro.ws.chk.ChkIncomeService/sendChk',
        $argument,
        ['\Com\Programika\Rro\Ws\Chk\CheckResponse', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Com\Programika\Rro\Ws\Chk\Check $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function sendChkV2(\Com\Programika\Rro\Ws\Chk\Check $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/com.programika.rro.ws.chk.ChkIncomeService/sendChkV2',
        $argument,
        ['\Com\Programika\Rro\Ws\Chk\CheckResponse', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Com\Programika\Rro\Ws\Chk\CheckRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function lastChk(\Com\Programika\Rro\Ws\Chk\CheckRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/com.programika.rro.ws.chk.ChkIncomeService/lastChk',
        $argument,
        ['\Com\Programika\Rro\Ws\Chk\CheckResponse', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Com\Programika\Rro\Ws\Chk\Check $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function ping(\Com\Programika\Rro\Ws\Chk\Check $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/com.programika.rro.ws.chk.ChkIncomeService/ping',
        $argument,
        ['\Com\Programika\Rro\Ws\Chk\CheckResponse', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Com\Programika\Rro\Ws\Chk\CheckRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function delLastChk(\Com\Programika\Rro\Ws\Chk\CheckRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/com.programika.rro.ws.chk.ChkIncomeService/delLastChk',
        $argument,
        ['\Com\Programika\Rro\Ws\Chk\CheckResponse', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Com\Programika\Rro\Ws\Chk\CheckRequestId $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function delLastChkId(\Com\Programika\Rro\Ws\Chk\CheckRequestId $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/com.programika.rro.ws.chk.ChkIncomeService/delLastChkId',
        $argument,
        ['\Com\Programika\Rro\Ws\Chk\CheckResponse', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Com\Programika\Rro\Ws\Chk\CheckRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function statusRro(\Com\Programika\Rro\Ws\Chk\CheckRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/com.programika.rro.ws.chk.ChkIncomeService/statusRro',
        $argument,
        ['\Com\Programika\Rro\Ws\Chk\StatusResponse', 'decode'],
        $metadata, $options);
    }

    /**
     * @param \Com\Programika\Rro\Ws\Chk\CheckRequest $argument input argument
     * @param array $metadata metadata
     * @param array $options call options
     */
    public function infoRro(\Com\Programika\Rro\Ws\Chk\CheckRequest $argument,
      $metadata = [], $options = []) {
        return $this->_simpleRequest('/com.programika.rro.ws.chk.ChkIncomeService/infoRro',
        $argument,
        ['\Com\Programika\Rro\Ws\Chk\RroInfoResponse', 'decode'],
        $metadata, $options);
    }

}
